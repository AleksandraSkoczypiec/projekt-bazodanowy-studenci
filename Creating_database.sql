/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2016                    */
/* Created on:     11.12.2017 19:06:18                          */
/*==============================================================*/


if exists (select 1
            from  sysindexes
           where  id    = object_id('Oceny_projekty')
            and   name  = 'Relationship_10_FK'
            and   indid > 0
            and   indid < 255)
   drop index Oceny_projekty.Relationship_10_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Oceny_projekty')
            and   name  = 'Relationship_6_FK'
            and   indid > 0
            and   indid < 255)
   drop index Oceny_projekty.Relationship_6_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Oceny_projekty')
            and   type = 'U')
   drop table Oceny_projekty
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Oceny_przedmioty')
            and   name  = 'Relationship_8_FK'
            and   indid > 0
            and   indid < 255)
   drop index Oceny_przedmioty.Relationship_8_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Oceny_przedmioty')
            and   name  = 'Relationship_7_FK'
            and   indid > 0
            and   indid < 255)
   drop index Oceny_przedmioty.Relationship_7_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Oceny_przedmioty')
            and   type = 'U')
   drop table Oceny_przedmioty
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Projekty')
            and   name  = 'Relationship5_FK'
            and   indid > 0
            and   indid < 255)
   drop index Projekty.Relationship5_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Projekty')
            and   type = 'U')
   drop table Projekty
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Prowadzacy')
            and   type = 'U')
   drop table Prowadzacy
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Przedmioty')
            and   name  = 'Relationship3_FK'
            and   indid > 0
            and   indid < 255)
   drop index Przedmioty.Relationship3_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Przedmioty')
            and   name  = 'Relationship4_FK'
            and   indid > 0
            and   indid < 255)
   drop index Przedmioty.Relationship4_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Przedmioty')
            and   type = 'U')
   drop table Przedmioty
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Specjalizacja')
            and   type = 'U')
   drop table Specjalizacja
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Studenci')
            and   name  = 'Relationship_9_FK'
            and   indid > 0
            and   indid < 255)
   drop index Studenci.Relationship_9_FK
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('Studenci')
            and   name  = 'Relationship2_FK'
            and   indid > 0
            and   indid < 255)
   drop index Studenci.Relationship2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Studenci')
            and   type = 'U')
   drop table Studenci
go

/*==============================================================*/
/* Table: Oceny_projekty                                        */
/*==============================================================*/
create table Oceny_projekty (
   OProj_Id             int                  IDENTITY(1,1) not null,
   ID_Projektu          int                  null,
   ID_Studenta          int                  null,
   OProj_Ocena          decimal(2,1)         null,
   OProj_Data           date                 null,
   constraint PK_OCENY_PROJEKTY primary key (OProj_Id)
)
go

/*==============================================================*/
/* Index: Relationship_6_FK                                     */
/*==============================================================*/




create nonclustered index Relationship_6_FK on Oceny_projekty (ID_Projektu ASC)
go

/*==============================================================*/
/* Index: Relationship_10_FK                                    */
/*==============================================================*/




create nonclustered index Relationship_10_FK on Oceny_projekty (ID_Studenta ASC)
go

/*==============================================================*/
/* Table: Oceny_przedmioty                                      */
/*==============================================================*/
create table Oceny_przedmioty (
   OPrz_Id              int                  IDENTITY(1,1) not null,
   ID_Studenta          int                  null,
   ID_przedmiotu        int                  null,
   OPrz_Ocena           decimal(2,1)         null,
   Oprz_Data            date                 null,
   constraint PK_OCENY_PRZEDMIOTY primary key (OPrz_Id)
)
go

/*==============================================================*/
/* Index: Relationship_7_FK                                     */
/*==============================================================*/




create nonclustered index Relationship_7_FK on Oceny_przedmioty (ID_przedmiotu ASC)
go

/*==============================================================*/
/* Index: Relationship_8_FK                                     */
/*==============================================================*/




create nonclustered index Relationship_8_FK on Oceny_przedmioty (ID_Studenta ASC)
go

/*==============================================================*/
/* Table: Projekty                                              */
/*==============================================================*/
create table Projekty (
   ID_Projektu          int                  IDENTITY(1,1) not null,
   ID_przedmiotu        int                  null,
   Nazwa_projektu       varchar(100)         not null,
   Data_projektu        date                 null,
   constraint PK_PROJEKTY primary key (ID_Projektu)
)
go

/*==============================================================*/
/* Index: Relationship5_FK                                      */
/*==============================================================*/




create nonclustered index Relationship5_FK on Projekty (ID_przedmiotu ASC)
go

/*==============================================================*/
/* Table: Prowadzacy                                            */
/*==============================================================*/
create table Prowadzacy (
   ID_prowadzacego      int                  IDENTITY(1,1) not null,
   P_Imie               varchar(20)          not null,
   P_Nazwisko           varchar(30)          not null,
   P_Stopien_naukowy    varchar(20)          null,
   P_Stanowisko         varchar(20)          not null,
   P_Adres              varchar(100)         null,
   constraint PK_PROWADZACY primary key (ID_prowadzacego)
)
go

/*==============================================================*/
/* Table: Przedmioty                                            */
/*==============================================================*/
create table Przedmioty (
   ID_przedmiotu        int                  IDENTITY(1,1) not null,
   ID_Specjalizacji     int                  null,
   ID_prowadzacego      int                  not null,
   Nazwa_przedmiotu     varchar(50)          not null,
   ECTS                 int                  not null,
   constraint PK_PRZEDMIOTY primary key (ID_przedmiotu)
)
go

/*==============================================================*/
/* Index: Relationship4_FK                                      */
/*==============================================================*/




create nonclustered index Relationship4_FK on Przedmioty (ID_prowadzacego ASC)
go

/*==============================================================*/
/* Index: Relationship3_FK                                      */
/*==============================================================*/




create nonclustered index Relationship3_FK on Przedmioty (ID_Specjalizacji ASC)
go

/*==============================================================*/
/* Table: Specjalizacja                                         */
/*==============================================================*/
create table Specjalizacja (
   ID_Specjalizacji     int                  IDENTITY(1,1) not null,
   Nazwa_specjalizacji  varchar(50)          not null,
   constraint PK_SPECJALIZACJA primary key (ID_Specjalizacji)
)
go

/*==============================================================*/
/* Table: Studenci                                              */
/*==============================================================*/
create table Studenci (
   ID_Studenta          int                  IDENTITY(1,1) not null,
   ID_Specjalizacji     int                  not null,
   ID_Projektu          int                  null,
   ST_Imie              varchar(20)          null,
   ST_Nazwisko          varchar(20)          not null,
   ST_Adres             varchar(100)         not null,
   ST_Nr_indeksu        numeric(5)           not null,
   ST_Data_urodzenia    date                 null,
   constraint PK_STUDENCI primary key (ID_Studenta)
)
go

/*==============================================================*/
/* Index: Relationship2_FK                                      */
/*==============================================================*/




create nonclustered index Relationship2_FK on Studenci (ID_Specjalizacji ASC)
go

/*==============================================================*/
/* Index: Relationship_9_FK                                     */
/*==============================================================*/




create nonclustered index Relationship_9_FK on Studenci (ID_Projektu ASC)
go

