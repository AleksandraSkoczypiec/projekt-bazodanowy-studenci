﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studenci
{
    public partial class Przedmioty_form : Form
    {
        StudenciEntities1 dbContext = new StudenciEntities1();
        
        public Przedmioty_form()
        {
            InitializeComponent();
            fillComboBox();
        }

        private void addButton_Click(object sender, EventArgs e)
        {

            if (subjectName.TextLength > 1 && ECTS.TextLength > 0)
            {
                var przedmiot = new Przedmioty
                {
                    Nazwa_przedmiotu = subjectName.Text,
                    ECTS = Int32.Parse(ECTS.Text),
                    ID_Specjalizacji = (int)specjalizacjaComboBox.SelectedValue,
                    ID_prowadzacego = (int)wykladowcaComboBox.SelectedValue
                    
                };

                
                dbContext.Przedmioty.Add(przedmiot);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Wypełnij wszystkie pola");
                return;
            }

            dbContext.SaveChanges();
            this.Close();
        }

        private void fillComboBox()
        {
            using (StudenciEntities1 c = new StudenciEntities1())
            {
                wykladowcaComboBox.ValueMember = "ID_prowadzacego";
                wykladowcaComboBox.DisplayMember = "P_Nazwisko";
                wykladowcaComboBox.DataSource = c.Prowadzacy.ToList();

                specjalizacjaComboBox.ValueMember = "ID_Specjalizacji";
                specjalizacjaComboBox.DisplayMember = "Nazwa_specjalizacji";
                specjalizacjaComboBox.DataSource = c.Specjalizacja.ToList();

               
            }


        }


    }
}
