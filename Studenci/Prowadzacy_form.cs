﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studenci
{
    public partial class Prowadzacy_form : Form
    {
        StudenciEntities1 dbContext = new StudenciEntities1();
        public Prowadzacy_form()
        {
            InitializeComponent();
        }

        private void addProwadzacy_Click(object sender, EventArgs e)
        {

            if (firstName.TextLength > 1 && lastName.TextLength > 1 && position.TextLength > 1)
            {
                var prowadzacy = new Prowadzacy
                {
                    P_Imie = firstName.Text,
                    P_Nazwisko = lastName.Text,
                    P_Stopien_naukowy = academicDegree.Text,
                    P_Stanowisko = position.Text,
                    P_Adres = address.Text


                };

                dbContext.Prowadzacy.Add(prowadzacy);
                dbContext.SaveChanges();

            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Wypełnij wszystkie pola");
                return;
            }
            
            this.Close();
        }


    }
}
