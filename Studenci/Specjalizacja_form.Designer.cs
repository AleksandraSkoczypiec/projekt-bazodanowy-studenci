﻿namespace Studenci
{
    partial class Specjalizacja_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.specName = new System.Windows.Forms.TextBox();
            this.specLabel = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // specName
            // 
            this.specName.Location = new System.Drawing.Point(167, 110);
            this.specName.Name = "specName";
            this.specName.Size = new System.Drawing.Size(176, 20);
            this.specName.TabIndex = 0;
            // 
            // specLabel
            // 
            this.specLabel.AutoSize = true;
            this.specLabel.Location = new System.Drawing.Point(45, 113);
            this.specLabel.Name = "specLabel";
            this.specLabel.Size = new System.Drawing.Size(102, 13);
            this.specLabel.TabIndex = 1;
            this.specLabel.Text = "Nazwa specjalizacji:";
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(189, 173);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 2;
            this.addButton.Text = "Dodaj";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // Specjalizacja_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 309);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.specLabel);
            this.Controls.Add(this.specName);
            this.Name = "Specjalizacja_form";
            this.Text = "Specjalizacja_form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox specName;
        private System.Windows.Forms.Label specLabel;
        private System.Windows.Forms.Button addButton;
    }
}