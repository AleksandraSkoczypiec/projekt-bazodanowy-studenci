﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studenci
{
    public partial class OcenaProjekt : Form
    {
        StudenciEntities1 dbContext = new StudenciEntities1();
        public OcenaProjekt()
        {
            InitializeComponent();
            loadData();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            var ocenaProjekt = new Oceny_projekty
            {
                ID_Studenta = (int)studentComboBox.SelectedValue,
                ID_Projektu = (int)projektComboBox.SelectedValue,
                OProj_Ocena = ocenaNumericUpDown.Value,
                OProj_Data = terminData.Value
            };

            dbContext.Oceny_projekty.Add(ocenaProjekt);
            dbContext.SaveChanges();
            this.Close();
        }

        private void loadData()
        {
            using (StudenciEntities1 c = new StudenciEntities1())
            {
                studentComboBox.ValueMember = "ID_Studenta";
                studentComboBox.DisplayMember = "ST_Nr_indeksu";
                studentComboBox.DataSource = c.Studenci.ToList();

                projektComboBox.ValueMember = "ID_Projektu";
                projektComboBox.DisplayMember = "Nazwa_projektu";
                projektComboBox.DataSource = c.Projekty.ToList();
            }
        }
    }
}
