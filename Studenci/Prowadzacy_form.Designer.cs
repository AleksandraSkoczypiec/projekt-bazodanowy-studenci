﻿namespace Studenci
{
    partial class Prowadzacy_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.firstName = new System.Windows.Forms.TextBox();
            this.lastName = new System.Windows.Forms.TextBox();
            this.academicDegree = new System.Windows.Forms.TextBox();
            this.position = new System.Windows.Forms.TextBox();
            this.address = new System.Windows.Forms.TextBox();
            this.labelImie = new System.Windows.Forms.Label();
            this.labelNazwisko = new System.Windows.Forms.Label();
            this.labelStopien = new System.Windows.Forms.Label();
            this.labelStanowisko = new System.Windows.Forms.Label();
            this.labelAdres = new System.Windows.Forms.Label();
            this.addProwadzacy = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // firstName
            // 
            this.firstName.Location = new System.Drawing.Point(159, 88);
            this.firstName.Name = "firstName";
            this.firstName.Size = new System.Drawing.Size(270, 20);
            this.firstName.TabIndex = 0;

            // lastName
            // 
            this.lastName.Location = new System.Drawing.Point(159, 115);
            this.lastName.Name = "lastName";
            this.lastName.Size = new System.Drawing.Size(270, 20);
            this.lastName.TabIndex = 1;
            // 
            // academicDegree
            // 
            this.academicDegree.Location = new System.Drawing.Point(159, 142);
            this.academicDegree.Name = "academicDegree";
            this.academicDegree.Size = new System.Drawing.Size(270, 20);
            this.academicDegree.TabIndex = 2;
            // 
            // position
            // 
            this.position.Location = new System.Drawing.Point(159, 169);
            this.position.Name = "position";
            this.position.Size = new System.Drawing.Size(270, 20);
            this.position.TabIndex = 3;
            // 
            // address
            // 
            this.address.Location = new System.Drawing.Point(159, 195);
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(270, 20);
            this.address.TabIndex = 4;
            // 
            // labelImie
            // 
            this.labelImie.AutoSize = true;
            this.labelImie.Location = new System.Drawing.Point(124, 91);
            this.labelImie.Name = "labelImie";
            this.labelImie.Size = new System.Drawing.Size(29, 13);
            this.labelImie.TabIndex = 5;
            this.labelImie.Text = "Imię:";
            this.labelImie.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelNazwisko
            // 
            this.labelNazwisko.AutoSize = true;
            this.labelNazwisko.Location = new System.Drawing.Point(97, 119);
            this.labelNazwisko.Name = "labelNazwisko";
            this.labelNazwisko.Size = new System.Drawing.Size(56, 13);
            this.labelNazwisko.TabIndex = 6;
            this.labelNazwisko.Text = "Nazwisko:";
            this.labelNazwisko.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelStopien
            // 
            this.labelStopien.AutoSize = true;
            this.labelStopien.Location = new System.Drawing.Point(61, 146);
            this.labelStopien.Name = "labelStopien";
            this.labelStopien.Size = new System.Drawing.Size(92, 13);
            this.labelStopien.TabIndex = 7;
            this.labelStopien.Text = "Stopień naukowy:";
            this.labelStopien.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelStanowisko
            // 
            this.labelStanowisko.AutoSize = true;
            this.labelStanowisko.Location = new System.Drawing.Point(88, 172);
            this.labelStanowisko.Name = "labelStanowisko";
            this.labelStanowisko.Size = new System.Drawing.Size(65, 13);
            this.labelStanowisko.TabIndex = 8;
            this.labelStanowisko.Text = "Stanowisko:";
            this.labelStanowisko.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelAdres
            // 
            this.labelAdres.AutoSize = true;
            this.labelAdres.Location = new System.Drawing.Point(116, 199);
            this.labelAdres.Name = "labelAdres";
            this.labelAdres.Size = new System.Drawing.Size(37, 13);
            this.labelAdres.TabIndex = 9;
            this.labelAdres.Text = "Adres:";
            this.labelAdres.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // addProwadzacy
            // 
            this.addProwadzacy.Location = new System.Drawing.Point(265, 291);
            this.addProwadzacy.Name = "addProwadzacy";
            this.addProwadzacy.Size = new System.Drawing.Size(75, 23);
            this.addProwadzacy.TabIndex = 10;
            this.addProwadzacy.Text = "Dodaj";
            this.addProwadzacy.UseVisualStyleBackColor = true;
            this.addProwadzacy.Click += new System.EventHandler(this.addProwadzacy_Click);
            // 
            // Prowadzacy_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 388);
            this.Controls.Add(this.addProwadzacy);
            this.Controls.Add(this.labelAdres);
            this.Controls.Add(this.labelStanowisko);
            this.Controls.Add(this.labelStopien);
            this.Controls.Add(this.labelNazwisko);
            this.Controls.Add(this.labelImie);
            this.Controls.Add(this.address);
            this.Controls.Add(this.position);
            this.Controls.Add(this.academicDegree);
            this.Controls.Add(this.lastName);
            this.Controls.Add(this.firstName);
            this.Name = "Prowadzacy_form";
            this.Text = "Prowadzacy_form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox firstName;
        private System.Windows.Forms.TextBox lastName;
        private System.Windows.Forms.TextBox academicDegree;
        private System.Windows.Forms.TextBox position;
        private System.Windows.Forms.TextBox address;
        private System.Windows.Forms.Label labelImie;
        private System.Windows.Forms.Label labelNazwisko;
        private System.Windows.Forms.Label labelStopien;
        private System.Windows.Forms.Label labelStanowisko;
        private System.Windows.Forms.Label labelAdres;
        private System.Windows.Forms.Button addProwadzacy;
    }
}