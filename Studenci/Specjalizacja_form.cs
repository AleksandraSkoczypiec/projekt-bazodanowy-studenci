﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studenci
{
    public partial class Specjalizacja_form : Form
    {
        StudenciEntities1 dbContext = new StudenciEntities1();

        public Specjalizacja_form()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            var specjalizacja = new Specjalizacja()
            {
                Nazwa_specjalizacji = specName.Text
            };

            dbContext.Specjalizacja.Add(specjalizacja);
            dbContext.SaveChanges();
            this.Close();

        }
    }
}
