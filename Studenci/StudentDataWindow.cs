﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studenci
{
    public partial class StudentDataWindow : Form
    {
        StudenciEntities1 dbContext = new StudenciEntities1();
        int currentUser;
        public StudentDataWindow(string user)
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = true;
          
            currentUser = Int32.Parse(user);

            //SELECT Studenci.ID_Studenta, Oceny_przedmioty.Oprz_Ocena FROM Oceny_przedmioty INNER JOIN Studenci ON Studenci.ID_Studenta =Oceny_przedmioty.ID_Studenta WHERE ST_Nr_indeksu = '11000'


            var query1 = dbContext.Oceny_przedmioty    
                .Join(dbContext.Studenci, 
                   post => post.ID_Studenta,        
                   meta => meta.ID_Studenta,   
                   (post, meta) => new { meta.ST_Nr_indeksu, post.OPrz_Ocena })
                 .Where(postAndMeta => postAndMeta.ST_Nr_indeksu == currentUser);    

            dataGridView1.DataSource = query1.ToList();
  

        }


    }
}
