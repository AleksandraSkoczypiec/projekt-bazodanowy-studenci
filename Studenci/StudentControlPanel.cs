﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studenci
{
    public partial class StudentControlPanel : Form
    {
        string currentUser;
        public StudentControlPanel(string user)
        {
            InitializeComponent();
            witajLabel.Text = "Witaj, " + user + ".";
            currentUser = user;
        }

        private void ocenaPrzedmiotButton_Click(object sender, EventArgs e)
        {
            StudentDataWindow studentDataWindow = new StudentDataWindow(currentUser);
            studentDataWindow.ShowDialog();

            //DataWindow dataWindow = new DataWindow(DbDataType.Ocena_przedmiot);
            //dataWindow.ShowDialog();
        }
    }
}
