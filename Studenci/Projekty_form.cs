﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studenci
{
    public partial class Projekty_form : Form
    {
        StudenciEntities1 dbContext = new StudenciEntities1();
        public Projekty_form()
        {
            InitializeComponent();
            fillComboBox();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            
            var projekt = new Projekty()
            {
                ID_przedmiotu = przedmiotComboBox.SelectedIndex,
                Nazwa_projektu = projName.Text,
                Data_projektu = terminDate.Value
            };

            dbContext.Projekty.Add(projekt);
            dbContext.SaveChanges();
            this.Close();
        }

        private void fillComboBox()
        {
            using (StudenciEntities1 c = new StudenciEntities1())
            {
                przedmiotComboBox.ValueMember = "ID_przedmiotu";
                przedmiotComboBox.DisplayMember = "Nazwa_przedmiotu";
                przedmiotComboBox.DataSource = c.Przedmioty.ToList();

            }
        }
    }
}
