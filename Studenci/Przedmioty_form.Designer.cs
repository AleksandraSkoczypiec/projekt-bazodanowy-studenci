﻿namespace Studenci
{
    partial class Przedmioty_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.subjectName = new System.Windows.Forms.TextBox();
            this.ECTS = new System.Windows.Forms.TextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.przedmiotNazwa = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.specjalizacjaComboBox = new System.Windows.Forms.ComboBox();
            this.wykladowcaComboBox = new System.Windows.Forms.ComboBox();
            this.specName = new System.Windows.Forms.Label();
            this.wyklName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // subjectName
            // 
            this.subjectName.Location = new System.Drawing.Point(153, 75);
            this.subjectName.Name = "subjectName";
            this.subjectName.Size = new System.Drawing.Size(182, 20);
            this.subjectName.TabIndex = 0;
            // 
            // ECTS
            // 
            this.ECTS.Location = new System.Drawing.Point(153, 102);
            this.ECTS.Name = "ECTS";
            this.ECTS.Size = new System.Drawing.Size(182, 20);
            this.ECTS.TabIndex = 1;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(195, 230);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 2;
            this.addButton.Text = "Dodaj";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // przedmiotNazwa
            // 
            this.przedmiotNazwa.AutoSize = true;
            this.przedmiotNazwa.Location = new System.Drawing.Point(50, 78);
            this.przedmiotNazwa.Name = "przedmiotNazwa";
            this.przedmiotNazwa.Size = new System.Drawing.Size(97, 13);
            this.przedmiotNazwa.TabIndex = 3;
            this.przedmiotNazwa.Text = "Nazwa przedmiotu:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(109, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "ECTS:";
            // 
            // specjalizacjaComboBox
            // 
            this.specjalizacjaComboBox.FormattingEnabled = true;
            this.specjalizacjaComboBox.Items.AddRange(new object[] {
            "Grafika",
            "Sieci komputerowe",
            "Informatyka w biznesie"});
            this.specjalizacjaComboBox.Location = new System.Drawing.Point(153, 129);
            this.specjalizacjaComboBox.Name = "specjalizacjaComboBox";
            this.specjalizacjaComboBox.Size = new System.Drawing.Size(182, 21);
            this.specjalizacjaComboBox.TabIndex = 5;
            // 
            // wykladowcaComboBox
            // 
            this.wykladowcaComboBox.FormattingEnabled = true;
            this.wykladowcaComboBox.Location = new System.Drawing.Point(153, 157);
            this.wykladowcaComboBox.Name = "wykladowcaComboBox";
            this.wykladowcaComboBox.Size = new System.Drawing.Size(182, 21);
            this.wykladowcaComboBox.TabIndex = 6;
            // 
            // specName
            // 
            this.specName.AutoSize = true;
            this.specName.Location = new System.Drawing.Point(75, 136);
            this.specName.Name = "specName";
            this.specName.Size = new System.Drawing.Size(72, 13);
            this.specName.TabIndex = 7;
            this.specName.Text = "Specjalizacja:";
            // 
            // wyklName
            // 
            this.wyklName.AutoSize = true;
            this.wyklName.Location = new System.Drawing.Point(73, 164);
            this.wyklName.Name = "wyklName";
            this.wyklName.Size = new System.Drawing.Size(74, 13);
            this.wyklName.TabIndex = 8;
            this.wyklName.Text = "Wykładowca:";
            // 
            // Przedmioty_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 318);
            this.Controls.Add(this.wyklName);
            this.Controls.Add(this.specName);
            this.Controls.Add(this.wykladowcaComboBox);
            this.Controls.Add(this.specjalizacjaComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.przedmiotNazwa);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.ECTS);
            this.Controls.Add(this.subjectName);
            this.Name = "Przedmioty_form";
            this.Text = "Przedmioty_form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox subjectName;
        private System.Windows.Forms.TextBox ECTS;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Label przedmiotNazwa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox specjalizacjaComboBox;
        private System.Windows.Forms.ComboBox wykladowcaComboBox;
        private System.Windows.Forms.Label specName;
        private System.Windows.Forms.Label wyklName;
    }
}