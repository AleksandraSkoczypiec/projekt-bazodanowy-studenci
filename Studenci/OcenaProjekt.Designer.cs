﻿namespace Studenci
{
    partial class OcenaProjekt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.projektComboBox = new System.Windows.Forms.ComboBox();
            this.terminData = new System.Windows.Forms.DateTimePicker();
            this.addButton = new System.Windows.Forms.Button();
            this.projLabel = new System.Windows.Forms.Label();
            this.ocenaProjektNumeric = new System.Windows.Forms.Label();
            this.terminLabel = new System.Windows.Forms.Label();
            this.studentComboBox = new System.Windows.Forms.ComboBox();
            this.studentLabel = new System.Windows.Forms.Label();
            this.ocenaNumericUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.ocenaNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // projektComboBox
            // 
            this.projektComboBox.FormattingEnabled = true;
            this.projektComboBox.Location = new System.Drawing.Point(170, 90);
            this.projektComboBox.Name = "projektComboBox";
            this.projektComboBox.Size = new System.Drawing.Size(200, 21);
            this.projektComboBox.TabIndex = 0;
            // 
            // terminData
            // 
            this.terminData.Location = new System.Drawing.Point(170, 145);
            this.terminData.Name = "terminData";
            this.terminData.Size = new System.Drawing.Size(200, 20);
            this.terminData.TabIndex = 2;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(244, 232);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 3;
            this.addButton.Text = "Dodaj";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // projLabel
            // 
            this.projLabel.AutoSize = true;
            this.projLabel.Location = new System.Drawing.Point(109, 93);
            this.projLabel.Name = "projLabel";
            this.projLabel.Size = new System.Drawing.Size(43, 13);
            this.projLabel.TabIndex = 4;
            this.projLabel.Text = "Projekt:";
            // 
            // ocenaProjektNumeric
            // 
            this.ocenaProjektNumeric.AutoSize = true;
            this.ocenaProjektNumeric.Location = new System.Drawing.Point(110, 121);
            this.ocenaProjektNumeric.Name = "ocenaProjektNumeric";
            this.ocenaProjektNumeric.Size = new System.Drawing.Size(42, 13);
            this.ocenaProjektNumeric.TabIndex = 5;
            this.ocenaProjektNumeric.Text = "Ocena:";
            // 
            // terminLabel
            // 
            this.terminLabel.AutoSize = true;
            this.terminLabel.Location = new System.Drawing.Point(110, 148);
            this.terminLabel.Name = "terminLabel";
            this.terminLabel.Size = new System.Drawing.Size(42, 13);
            this.terminLabel.TabIndex = 6;
            this.terminLabel.Text = "Termin:";
            // 
            // studentComboBox
            // 
            this.studentComboBox.FormattingEnabled = true;
            this.studentComboBox.Location = new System.Drawing.Point(170, 63);
            this.studentComboBox.Name = "studentComboBox";
            this.studentComboBox.Size = new System.Drawing.Size(200, 21);
            this.studentComboBox.TabIndex = 7;
            // 
            // studentLabel
            // 
            this.studentLabel.AutoSize = true;
            this.studentLabel.Location = new System.Drawing.Point(105, 67);
            this.studentLabel.Name = "studentLabel";
            this.studentLabel.Size = new System.Drawing.Size(47, 13);
            this.studentLabel.TabIndex = 8;
            this.studentLabel.Text = "Student:";
            // 
            // ocenaNumericUpDown
            // 
            this.ocenaNumericUpDown.DecimalPlaces = 1;
            this.ocenaNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.ocenaNumericUpDown.Location = new System.Drawing.Point(170, 119);
            this.ocenaNumericUpDown.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.ocenaNumericUpDown.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.ocenaNumericUpDown.Name = "ocenaNumericUpDown";
            this.ocenaNumericUpDown.Size = new System.Drawing.Size(200, 20);
            this.ocenaNumericUpDown.TabIndex = 9;
            this.ocenaNumericUpDown.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // OcenaProjekt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 402);
            this.Controls.Add(this.ocenaNumericUpDown);
            this.Controls.Add(this.studentLabel);
            this.Controls.Add(this.studentComboBox);
            this.Controls.Add(this.terminLabel);
            this.Controls.Add(this.ocenaProjektNumeric);
            this.Controls.Add(this.projLabel);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.terminData);
            this.Controls.Add(this.projektComboBox);
            this.Name = "OcenaProjekt";
            this.Text = "OcenaProjekt";
            ((System.ComponentModel.ISupportInitialize)(this.ocenaNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox projektComboBox;
        private System.Windows.Forms.DateTimePicker terminData;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Label projLabel;
        private System.Windows.Forms.Label ocenaProjektNumeric;
        private System.Windows.Forms.Label terminLabel;
        private System.Windows.Forms.ComboBox studentComboBox;
        private System.Windows.Forms.Label studentLabel;
        private System.Windows.Forms.NumericUpDown ocenaNumericUpDown;
    }
}