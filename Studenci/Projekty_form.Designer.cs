﻿namespace Studenci
{
    partial class Projekty_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.projName = new System.Windows.Forms.TextBox();
            this.projectLabel = new System.Windows.Forms.Label();
            this.terminLabel = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.terminDate = new System.Windows.Forms.DateTimePicker();
            this.przedmiotComboBox = new System.Windows.Forms.ComboBox();
            this.przedmiotLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // projName
            // 
            this.projName.Location = new System.Drawing.Point(137, 114);
            this.projName.Name = "projName";
            this.projName.Size = new System.Drawing.Size(200, 20);
            this.projName.TabIndex = 0;
            // 
            // projectLabel
            // 
            this.projectLabel.AutoSize = true;
            this.projectLabel.Location = new System.Drawing.Point(47, 117);
            this.projectLabel.Name = "projectLabel";
            this.projectLabel.Size = new System.Drawing.Size(84, 13);
            this.projectLabel.TabIndex = 2;
            this.projectLabel.Text = "Nazwa projektu:";
            // 
            // terminLabel
            // 
            this.terminLabel.AutoSize = true;
            this.terminLabel.Location = new System.Drawing.Point(89, 143);
            this.terminLabel.Name = "terminLabel";
            this.terminLabel.Size = new System.Drawing.Size(42, 13);
            this.terminLabel.TabIndex = 3;
            this.terminLabel.Text = "Termin:";
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(195, 233);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 4;
            this.addButton.Text = "Dodaj";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // terminDate
            // 
            this.terminDate.Location = new System.Drawing.Point(137, 140);
            this.terminDate.Name = "terminDate";
            this.terminDate.Size = new System.Drawing.Size(200, 20);
            this.terminDate.TabIndex = 5;
            // 
            // przedmiotComboBox
            // 
            this.przedmiotComboBox.FormattingEnabled = true;
            this.przedmiotComboBox.Location = new System.Drawing.Point(137, 87);
            this.przedmiotComboBox.Name = "przedmiotComboBox";
            this.przedmiotComboBox.Size = new System.Drawing.Size(200, 21);
            this.przedmiotComboBox.TabIndex = 6;
            // 
            // przedmiotLabel
            // 
            this.przedmiotLabel.AutoSize = true;
            this.przedmiotLabel.Location = new System.Drawing.Point(34, 87);
            this.przedmiotLabel.Name = "przedmiotLabel";
            this.przedmiotLabel.Size = new System.Drawing.Size(97, 13);
            this.przedmiotLabel.TabIndex = 7;
            this.przedmiotLabel.Text = "Nazwa przedmiotu:";
            // 
            // Projekty_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 375);
            this.Controls.Add(this.przedmiotLabel);
            this.Controls.Add(this.przedmiotComboBox);
            this.Controls.Add(this.terminDate);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.terminLabel);
            this.Controls.Add(this.projectLabel);
            this.Controls.Add(this.projName);
            this.Name = "Projekty_form";
            this.Text = "Projekty_form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox projName;
        private System.Windows.Forms.Label projectLabel;
        private System.Windows.Forms.Label terminLabel;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.DateTimePicker terminDate;
        private System.Windows.Forms.ComboBox przedmiotComboBox;
        private System.Windows.Forms.Label przedmiotLabel;
    }
}