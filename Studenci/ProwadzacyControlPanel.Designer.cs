﻿namespace Studenci
{
    partial class ProwadzacyControlPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tematProjektuButton = new System.Windows.Forms.Button();
            this.ocenyPrzedmiotButton = new System.Windows.Forms.Button();
            this.ocenyProjektButton = new System.Windows.Forms.Button();
            this.wylogujButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tematProjektuButton
            // 
            this.tematProjektuButton.Location = new System.Drawing.Point(111, 229);
            this.tematProjektuButton.Name = "tematProjektuButton";
            this.tematProjektuButton.Size = new System.Drawing.Size(117, 55);
            this.tematProjektuButton.TabIndex = 0;
            this.tematProjektuButton.Text = "Wprowadz temat projektu";
            this.tematProjektuButton.UseVisualStyleBackColor = true;
            this.tematProjektuButton.Click += new System.EventHandler(this.tematProjektuButton_Click);
            // 
            // ocenyPrzedmiotButton
            // 
            this.ocenyPrzedmiotButton.Location = new System.Drawing.Point(357, 229);
            this.ocenyPrzedmiotButton.Name = "ocenyPrzedmiotButton";
            this.ocenyPrzedmiotButton.Size = new System.Drawing.Size(117, 55);
            this.ocenyPrzedmiotButton.TabIndex = 1;
            this.ocenyPrzedmiotButton.Text = "Wprowadź oceny z przedmiotu";
            this.ocenyPrzedmiotButton.UseVisualStyleBackColor = true;
            this.ocenyPrzedmiotButton.Click += new System.EventHandler(this.ocenyPrzedmiotButton_Click);
            // 
            // ocenyProjektButton
            // 
            this.ocenyProjektButton.Location = new System.Drawing.Point(234, 229);
            this.ocenyProjektButton.Name = "ocenyProjektButton";
            this.ocenyProjektButton.Size = new System.Drawing.Size(117, 55);
            this.ocenyProjektButton.TabIndex = 2;
            this.ocenyProjektButton.Text = "Wprowadź oceny z projektu";
            this.ocenyProjektButton.UseVisualStyleBackColor = true;
            this.ocenyProjektButton.Click += new System.EventHandler(this.ocenyProjektButton_Click);
            // 
            // wylogujButton
            // 
            this.wylogujButton.Location = new System.Drawing.Point(521, 22);
            this.wylogujButton.Name = "wylogujButton";
            this.wylogujButton.Size = new System.Drawing.Size(75, 23);
            this.wylogujButton.TabIndex = 3;
            this.wylogujButton.Text = "Wyloguj";
            this.wylogujButton.UseVisualStyleBackColor = true;
            // 
            // ProwadzacyControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 450);
            this.Controls.Add(this.wylogujButton);
            this.Controls.Add(this.ocenyProjektButton);
            this.Controls.Add(this.ocenyPrzedmiotButton);
            this.Controls.Add(this.tematProjektuButton);
            this.Name = "ProwadzacyControlPanel";
            this.Text = "ProwadzacyControlPanel";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button tematProjektuButton;
        private System.Windows.Forms.Button ocenyPrzedmiotButton;
        private System.Windows.Forms.Button ocenyProjektButton;
        private System.Windows.Forms.Button wylogujButton;
    }
}