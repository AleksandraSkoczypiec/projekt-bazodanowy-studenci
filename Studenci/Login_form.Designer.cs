﻿namespace Studenci
{
    partial class Login_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userName = new System.Windows.Forms.TextBox();
            this.loginButton = new System.Windows.Forms.Button();
            this.userLabel = new System.Windows.Forms.Label();
            this.passName = new System.Windows.Forms.TextBox();
            this.passLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // userName
            // 
            this.userName.Location = new System.Drawing.Point(126, 68);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(210, 20);
            this.userName.TabIndex = 0;
            // 
            // loginButton
            // 
            this.loginButton.Location = new System.Drawing.Point(269, 166);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(123, 60);
            this.loginButton.TabIndex = 1;
            this.loginButton.Text = "Zaloguj";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // userLabel
            // 
            this.userLabel.AutoSize = true;
            this.userLabel.Location = new System.Drawing.Point(43, 75);
            this.userLabel.Name = "userLabel";
            this.userLabel.Size = new System.Drawing.Size(65, 13);
            this.userLabel.TabIndex = 2;
            this.userLabel.Text = "Użytkownik:";
            // 
            // passName
            // 
            this.passName.Location = new System.Drawing.Point(126, 94);
            this.passName.Name = "passName";
            this.passName.Size = new System.Drawing.Size(210, 20);
            this.passName.TabIndex = 0;
            this.passName.UseSystemPasswordChar = true;
            // 
            // passLabel
            // 
            this.passLabel.AutoSize = true;
            this.passLabel.Location = new System.Drawing.Point(43, 101);
            this.passLabel.Name = "passLabel";
            this.passLabel.Size = new System.Drawing.Size(39, 13);
            this.passLabel.TabIndex = 2;
            this.passLabel.Text = "Hasło:";
            // 
            // Login_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 367);
            this.Controls.Add(this.passLabel);
            this.Controls.Add(this.userLabel);
            this.Controls.Add(this.loginButton);
            this.Controls.Add(this.passName);
            this.Controls.Add(this.userName);
            this.Name = "Login_form";
            this.Text = "Login_form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox userName;
        private System.Windows.Forms.Button loginButton;
        private System.Windows.Forms.Label userLabel;
        private System.Windows.Forms.TextBox passName;
        private System.Windows.Forms.Label passLabel;
    }
}