﻿namespace Studenci
{
    partial class OcenaPrzedmiot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.studentComboBox = new System.Windows.Forms.ComboBox();
            this.przedmiotComboBox = new System.Windows.Forms.ComboBox();
            this.ocenaNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.dataOceny = new System.Windows.Forms.DateTimePicker();
            this.addButton = new System.Windows.Forms.Button();
            this.studentLabel = new System.Windows.Forms.Label();
            this.przedmiotLabel = new System.Windows.Forms.Label();
            this.ocenaLabel = new System.Windows.Forms.Label();
            this.dataLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ocenaNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // studentComboBox
            // 
            this.studentComboBox.FormattingEnabled = true;
            this.studentComboBox.Location = new System.Drawing.Point(158, 134);
            this.studentComboBox.Name = "studentComboBox";
            this.studentComboBox.Size = new System.Drawing.Size(201, 21);
            this.studentComboBox.TabIndex = 0;
            // 
            // przedmiotComboBox
            // 
            this.przedmiotComboBox.FormattingEnabled = true;
            this.przedmiotComboBox.Location = new System.Drawing.Point(158, 162);
            this.przedmiotComboBox.Name = "przedmiotComboBox";
            this.przedmiotComboBox.Size = new System.Drawing.Size(201, 21);
            this.przedmiotComboBox.TabIndex = 1;
            // 
            // ocenaNumericUpDown
            // 
            this.ocenaNumericUpDown.DecimalPlaces = 1;
            this.ocenaNumericUpDown.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.ocenaNumericUpDown.Location = new System.Drawing.Point(158, 190);
            this.ocenaNumericUpDown.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.ocenaNumericUpDown.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.ocenaNumericUpDown.Name = "ocenaNumericUpDown";
            this.ocenaNumericUpDown.Size = new System.Drawing.Size(200, 20);
            this.ocenaNumericUpDown.TabIndex = 2;
            this.ocenaNumericUpDown.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // dataOceny
            // 
            this.dataOceny.Location = new System.Drawing.Point(158, 217);
            this.dataOceny.Name = "dataOceny";
            this.dataOceny.Size = new System.Drawing.Size(200, 20);
            this.dataOceny.TabIndex = 3;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(237, 298);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 4;
            this.addButton.Text = "Dodaj";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // studentLabel
            // 
            this.studentLabel.AutoSize = true;
            this.studentLabel.Location = new System.Drawing.Point(96, 137);
            this.studentLabel.Name = "studentLabel";
            this.studentLabel.Size = new System.Drawing.Size(47, 13);
            this.studentLabel.TabIndex = 5;
            this.studentLabel.Text = "Student:";
            // 
            // przedmiotLabel
            // 
            this.przedmiotLabel.AutoSize = true;
            this.przedmiotLabel.Location = new System.Drawing.Point(87, 167);
            this.przedmiotLabel.Name = "przedmiotLabel";
            this.przedmiotLabel.Size = new System.Drawing.Size(56, 13);
            this.przedmiotLabel.TabIndex = 6;
            this.przedmiotLabel.Text = "Przedmiot:";
            // 
            // ocenaLabel
            // 
            this.ocenaLabel.AutoSize = true;
            this.ocenaLabel.Location = new System.Drawing.Point(101, 194);
            this.ocenaLabel.Name = "ocenaLabel";
            this.ocenaLabel.Size = new System.Drawing.Size(42, 13);
            this.ocenaLabel.TabIndex = 7;
            this.ocenaLabel.Text = "Ocena:";
            // 
            // dataLabel
            // 
            this.dataLabel.AutoSize = true;
            this.dataLabel.Location = new System.Drawing.Point(78, 222);
            this.dataLabel.Name = "dataLabel";
            this.dataLabel.Size = new System.Drawing.Size(65, 13);
            this.dataLabel.TabIndex = 8;
            this.dataLabel.Text = "Data oceny:";
            // 
            // OcenaPrzedmiot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 481);
            this.Controls.Add(this.dataLabel);
            this.Controls.Add(this.ocenaLabel);
            this.Controls.Add(this.przedmiotLabel);
            this.Controls.Add(this.studentLabel);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.dataOceny);
            this.Controls.Add(this.ocenaNumericUpDown);
            this.Controls.Add(this.przedmiotComboBox);
            this.Controls.Add(this.studentComboBox);
            this.Name = "OcenaPrzedmiot";
            this.Text = "OcenaPrzedmiot";
            ((System.ComponentModel.ISupportInitialize)(this.ocenaNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox studentComboBox;
        private System.Windows.Forms.ComboBox przedmiotComboBox;
        private System.Windows.Forms.NumericUpDown ocenaNumericUpDown;
        private System.Windows.Forms.DateTimePicker dataOceny;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Label studentLabel;
        private System.Windows.Forms.Label przedmiotLabel;
        private System.Windows.Forms.Label ocenaLabel;
        private System.Windows.Forms.Label dataLabel;
    }
}