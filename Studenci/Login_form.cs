﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data.Sql;
using System.Linq;

namespace Studenci
{
    public partial class Login_form : Form
    {
        StudenciEntities1 dbContext = new StudenciEntities1();
        
        public Login_form()
        {
            InitializeComponent();
        }


        private void loginButton_Click(object sender, EventArgs e)
        {
            //string providedUsername, providedPassword;

            using (var context = new StudenciEntities1())
            {
                try
                {
                    var result = context.Login.First(c => c.Username == userName.Text && c.Password == passName.Text);
                    if(result.Role == "admin")
                    {
                        MainWindow mainWindow = new MainWindow(result.Role);
                        mainWindow.Show();

                    }else if (result.Role == "student")
                    {
                        StudentControlPanel studentControlPanel = new StudentControlPanel(userName.Text);
                        studentControlPanel.Show();

                    }else if(result.Role == "prowadzacy")
                    {
                        ProwadzacyControlPanel prowadzacyControlPanel = new ProwadzacyControlPanel();
                        prowadzacyControlPanel.Show();
                    }

                }
                catch (Exception)
                {
                    MessageBox.Show("Niepoprawne dane");
                    return;

                }
            };

            
        }
    }
}
