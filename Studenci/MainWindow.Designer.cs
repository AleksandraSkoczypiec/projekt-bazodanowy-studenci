﻿namespace Studenci
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.studentControlButton = new System.Windows.Forms.Button();
            this.prowadzacyControlButton = new System.Windows.Forms.Button();
            this.przedmiotControlButton = new System.Windows.Forms.Button();
            this.logoutButton = new System.Windows.Forms.Button();
            this.studenciBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.specjalizacjaControlButton = new System.Windows.Forms.Button();
            this.projektyControlButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.studenciBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(284, 40);
            this.label1.MaximumSize = new System.Drawing.Size(60, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "status";
            // 
            // studentControlButton
            // 
            this.studentControlButton.Location = new System.Drawing.Point(77, 153);
            this.studentControlButton.Name = "studentControlButton";
            this.studentControlButton.Size = new System.Drawing.Size(145, 60);
            this.studentControlButton.TabIndex = 4;
            this.studentControlButton.Text = "Zarzadzanie studentami";
            this.studentControlButton.UseVisualStyleBackColor = true;
            this.studentControlButton.Click += new System.EventHandler(this.studentControlButton_Click);
            // 
            // prowadzacyControlButton
            // 
            this.prowadzacyControlButton.Location = new System.Drawing.Point(237, 153);
            this.prowadzacyControlButton.Name = "prowadzacyControlButton";
            this.prowadzacyControlButton.Size = new System.Drawing.Size(145, 60);
            this.prowadzacyControlButton.TabIndex = 5;
            this.prowadzacyControlButton.Text = "Zarządzanie Prowadzącymi";
            this.prowadzacyControlButton.UseVisualStyleBackColor = true;
            this.prowadzacyControlButton.Click += new System.EventHandler(this.prowadzacyControlButton_Click);
            // 
            // przedmiotControlButton
            // 
            this.przedmiotControlButton.Location = new System.Drawing.Point(388, 153);
            this.przedmiotControlButton.Name = "przedmiotControlButton";
            this.przedmiotControlButton.Size = new System.Drawing.Size(145, 60);
            this.przedmiotControlButton.TabIndex = 6;
            this.przedmiotControlButton.Text = "Zarządzanie Przedmiotami";
            this.przedmiotControlButton.UseVisualStyleBackColor = true;
            this.przedmiotControlButton.Click += new System.EventHandler(this.przedmiotControlButton_Click);
            // 
            // logoutButton
            // 
            this.logoutButton.Location = new System.Drawing.Point(561, 12);
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Size = new System.Drawing.Size(75, 23);
            this.logoutButton.TabIndex = 7;
            this.logoutButton.Text = "Wyloguj";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // specjalizacjaControlButton
            // 
            this.specjalizacjaControlButton.Location = new System.Drawing.Point(77, 220);
            this.specjalizacjaControlButton.Name = "specjalizacjaControlButton";
            this.specjalizacjaControlButton.Size = new System.Drawing.Size(145, 60);
            this.specjalizacjaControlButton.TabIndex = 8;
            this.specjalizacjaControlButton.Text = "Zarządzanie Specjalizacjami";
            this.specjalizacjaControlButton.UseVisualStyleBackColor = true;
            this.specjalizacjaControlButton.Click += new System.EventHandler(this.specjalizacjaControlButton_Click);
            // 
            // projektyControlButton
            // 
            this.projektyControlButton.Location = new System.Drawing.Point(237, 220);
            this.projektyControlButton.Name = "projektyControlButton";
            this.projektyControlButton.Size = new System.Drawing.Size(145, 60);
            this.projektyControlButton.TabIndex = 9;
            this.projektyControlButton.Text = "Zarządzenie Projektami";
            this.projektyControlButton.UseVisualStyleBackColor = true;
            this.projektyControlButton.Click += new System.EventHandler(this.projektyControlButton_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 396);
            this.Controls.Add(this.projektyControlButton);
            this.Controls.Add(this.specjalizacjaControlButton);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.przedmiotControlButton);
            this.Controls.Add(this.prowadzacyControlButton);
            this.Controls.Add(this.studentControlButton);
            this.Controls.Add(this.label1);
            this.Name = "MainWindow";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.studenciBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource studenciBindingSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button studentControlButton;
        private System.Windows.Forms.Button prowadzacyControlButton;
        private System.Windows.Forms.Button przedmiotControlButton;
        private System.Windows.Forms.Button logoutButton;
        private System.Windows.Forms.Button specjalizacjaControlButton;
        private System.Windows.Forms.Button projektyControlButton;
    }
}