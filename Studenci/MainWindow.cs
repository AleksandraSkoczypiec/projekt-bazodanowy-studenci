﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Studenci
{
    public partial class MainWindow : Form
    {
        StudenciEntities1 dbContext = new StudenciEntities1();
        List<string> optionsStudent = new List<string> {"Dodaj studenta", "Wyświetl studenta", "Edytuj studenta", "Usuń studenta" };
        List<string> optionsWykladowca = new List<string> { "Dodaj wykładowcę", "Wyświetl wykładowców", "Edytuj wykładowcę" };

        public MainWindow(string role)
        {
            InitializeComponent();
            label1.Text = "Witaj, " + role + "!";
            
        }

        public void loadData()
        {
            studenciBindingSource.DataSource = dbContext.Studenci.ToList();
        
        }

        private void studentControlButton_Click(object sender, EventArgs e)
        {
            DataWindow dataWindow = new DataWindow(DbDataType.Studenci);
            dataWindow.Show();
        }

        private void prowadzacyControlButton_Click(object sender, EventArgs e)
        {
            DataWindow dataWindow = new DataWindow(DbDataType.Prowadzacy);
            dataWindow.Show();
        }

        private void przedmiotControlButton_Click(object sender, EventArgs e)
        {
            DataWindow dataWindow = new DataWindow(DbDataType.Przedmioty);
            dataWindow.Show();

        }

        private void specjalizacjaControlButton_Click(object sender, EventArgs e)
        {
            DataWindow dataWindow = new DataWindow(DbDataType.Specjalizacje);
            dataWindow.Show();
        }

        private void projektyControlButton_Click(object sender, EventArgs e)
        {
            DataWindow dataWindow = new DataWindow(DbDataType.Projekty);
            dataWindow.Show();
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            Login_form login_ = new Login_form();
            this.Close();
            login_.Show();
        }
    }
}