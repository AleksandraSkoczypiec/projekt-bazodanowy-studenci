﻿namespace Studenci
{
    partial class Udzial_projekt_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.projektComboBox = new System.Windows.Forms.ComboBox();
            this.studentComboBox = new System.Windows.Forms.ComboBox();
            this.studentLabel = new System.Windows.Forms.Label();
            this.projektLabel = new System.Windows.Forms.Label();
            this.dodajButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // projektComboBox
            // 
            this.projektComboBox.FormattingEnabled = true;
            this.projektComboBox.Location = new System.Drawing.Point(100, 183);
            this.projektComboBox.Name = "projektComboBox";
            this.projektComboBox.Size = new System.Drawing.Size(200, 21);
            this.projektComboBox.TabIndex = 0;
            // 
            // studentComboBox
            // 
            this.studentComboBox.FormattingEnabled = true;
            this.studentComboBox.Location = new System.Drawing.Point(100, 156);
            this.studentComboBox.Name = "studentComboBox";
            this.studentComboBox.Size = new System.Drawing.Size(200, 21);
            this.studentComboBox.TabIndex = 1;
            // 
            // studentLabel
            // 
            this.studentLabel.AutoSize = true;
            this.studentLabel.Location = new System.Drawing.Point(46, 163);
            this.studentLabel.Name = "studentLabel";
            this.studentLabel.Size = new System.Drawing.Size(47, 13);
            this.studentLabel.TabIndex = 2;
            this.studentLabel.Text = "Student:";
            // 
            // projektLabel
            // 
            this.projektLabel.AutoSize = true;
            this.projektLabel.Location = new System.Drawing.Point(49, 190);
            this.projektLabel.Name = "projektLabel";
            this.projektLabel.Size = new System.Drawing.Size(43, 13);
            this.projektLabel.TabIndex = 3;
            this.projektLabel.Text = "Projekt:";
            // 
            // dodajButton
            // 
            this.dodajButton.Location = new System.Drawing.Point(177, 261);
            this.dodajButton.Name = "dodajButton";
            this.dodajButton.Size = new System.Drawing.Size(75, 23);
            this.dodajButton.TabIndex = 4;
            this.dodajButton.Text = "Dodaj";
            this.dodajButton.UseVisualStyleBackColor = true;
            // 
            // Udzial_projekt_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(416, 397);
            this.Controls.Add(this.dodajButton);
            this.Controls.Add(this.projektLabel);
            this.Controls.Add(this.studentLabel);
            this.Controls.Add(this.studentComboBox);
            this.Controls.Add(this.projektComboBox);
            this.Name = "Udzial_projekt_form";
            this.Text = "Udzial_projekt_form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox projektComboBox;
        private System.Windows.Forms.ComboBox studentComboBox;
        private System.Windows.Forms.Label studentLabel;
        private System.Windows.Forms.Label projektLabel;
        private System.Windows.Forms.Button dodajButton;
    }
}