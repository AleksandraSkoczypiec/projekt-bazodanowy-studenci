﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studenci
{
    public partial class Studenci_form : Form 
    {
        StudenciEntities1 dbContext = new StudenciEntities1();
        
        public Studenci_form()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var student = new Studenci
                {
                    ST_Imie = firstName.Text,
                    ST_Nazwisko = lastName.Text,
                    ST_Adres = address.Text,
                    ST_Data_urodzenia = dateOfBirth.Value,
                    ST_Nr_indeksu = Decimal.Parse(indexNumber.Text),
                    
                };
                var specjalizacja = new Specjalizacja
                {
                    Nazwa_specjalizacji = specComboBox.Text
                };


                dbContext.Studenci.Add(student);
                dbContext.Specjalizacja.Add(specjalizacja);

            }catch (FormatException)
            {
                System.Windows.Forms.MessageBox.Show("Wypełnij wszystkie pola");
                return;
            }
            dbContext.SaveChanges();
            this.Close();
        }
    }
}
