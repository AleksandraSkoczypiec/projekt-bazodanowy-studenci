﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Studenci
{
    public partial class DataWindow : Form
    {
        StudenciEntities1 dbContext = new StudenciEntities1();
        DbDataType choice;
        DataGridViewRow row;
        Int32 cellClick;

        public DataWindow(DbDataType dbDataType)
        {

            InitializeComponent();
            dataGridView1.AutoGenerateColumns = true;
            choice = dbDataType;

            switch (dbDataType)
            {
                case DbDataType.Studenci:
                    studenciBindingSource.DataSource = dbContext.Studenci.ToList();
                    break;
                case DbDataType.Prowadzacy:
                    studenciBindingSource.DataSource = dbContext.Prowadzacy.ToList();
                    break;
                case DbDataType.Przedmioty:
                    studenciBindingSource.DataSource = dbContext.Przedmioty.ToList();
                    break;
                case DbDataType.Specjalizacje:
                    studenciBindingSource.DataSource = dbContext.Specjalizacja.ToList();
                    break;
                case DbDataType.Projekty:
                    studenciBindingSource.DataSource = dbContext.Projekty.ToList();
                    break;
                case DbDataType.Ocena_przedmiot:
                    studenciBindingSource.DataSource = dbContext.Oceny_przedmioty.ToList();
                    break;
                case DbDataType.Ocena_projekt:
                    studenciBindingSource.DataSource = dbContext.Oceny_projekty.ToList();
                    break;
            }

        }
        private void addButton_Click(object sender, EventArgs e)
        {
            switch (choice)
            {
                case DbDataType.Studenci:
                    Studenci_form studenci_ = new Studenci_form();
                    studenci_.ShowDialog();
                    studenciBindingSource.DataSource = dbContext.Studenci.ToList();

                    break;
                case DbDataType.Prowadzacy:
                    Prowadzacy_form prowadzacy_ = new Prowadzacy_form();
                    prowadzacy_.ShowDialog();
                    studenciBindingSource.DataSource = dbContext.Prowadzacy.ToList();
                    break;
                case DbDataType.Przedmioty:
                    Przedmioty_form przedmioty_ = new Przedmioty_form();
                    przedmioty_.ShowDialog();
                    studenciBindingSource.DataSource = dbContext.Przedmioty.ToList();
                    break;
                case DbDataType.Specjalizacje:
                    Specjalizacja_form specjalizacja_ = new Specjalizacja_form();
                    specjalizacja_.ShowDialog();
                    studenciBindingSource.DataSource = dbContext.Specjalizacja.ToList();
                    break;
                case DbDataType.Projekty:
                    Projekty_form projekty_ = new Projekty_form();
                    projekty_.ShowDialog();
                    studenciBindingSource.DataSource = dbContext.Projekty.ToList();
                    break;
                case DbDataType.Ocena_przedmiot:
                    OcenaPrzedmiot ocenyPrzedmiot = new OcenaPrzedmiot();
                    ocenyPrzedmiot.ShowDialog();
                    studenciBindingSource.DataSource = dbContext.Oceny_przedmioty.ToList();
                    break;
                case DbDataType.Ocena_projekt:
                    OcenaProjekt ocenyProjekt = new OcenaProjekt();
                    ocenyProjekt.ShowDialog();
                    studenciBindingSource.DataSource = dbContext.Oceny_projekty.ToList();
                    break;

            }
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            switch (choice)
            {
                case DbDataType.Studenci:
                    studenciBindingSource.DataSource = dbContext.Studenci.ToList();
                    break;
                case DbDataType.Prowadzacy:
                    studenciBindingSource.DataSource = dbContext.Prowadzacy.ToList();
                    break;
                case DbDataType.Przedmioty:
                    studenciBindingSource.DataSource = dbContext.Przedmioty.ToList();
                    break;
                case DbDataType.Specjalizacje:
                    studenciBindingSource.DataSource = dbContext.Specjalizacja.ToList();
                    break;
                case DbDataType.Projekty:
                    studenciBindingSource.DataSource = dbContext.Projekty.ToList();
                    break;
                case DbDataType.Ocena_przedmiot:
                    studenciBindingSource.DataSource = dbContext.Oceny_przedmioty.ToList();
                    break;
                case DbDataType.Ocena_projekt:
                    studenciBindingSource.DataSource = dbContext.Oceny_projekty.ToList();
                    break;
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            switch (choice)
            {
                case DbDataType.Studenci:
                    deleteStudent();
                    break;
                case DbDataType.Prowadzacy:
                    deleteProwadzacy();
                    break;
                case DbDataType.Przedmioty:
                    deletePrzedmiot();
                    break;
                case DbDataType.Specjalizacje:
                    deleteSpecjalizacja();
                    break;
                case DbDataType.Projekty:
                    deleteProjekt();
                    break;
                case DbDataType.Ocena_przedmiot:
                    deleteOcenaPrzedmiot();
                    break;
                case DbDataType.Ocena_projekt:
                    deleteOcenaProjekt();
                    break;
            }

        }
        private void deleteStudent()
        {

            int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            var student = dbContext.Studenci.First(c => c.ID_Studenta == id);
            dbContext.Studenci.Remove(student);
            dbContext.SaveChanges();
            studenciBindingSource.DataSource = dbContext.Studenci.ToList();

        }

        private void deleteProwadzacy()
        {
            int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            var prowadzacy = dbContext.Prowadzacy.First(c => c.ID_prowadzacego == id);
            dbContext.Prowadzacy.Remove(prowadzacy);
            dbContext.SaveChanges();
            studenciBindingSource.DataSource = dbContext.Prowadzacy.ToList();
        }

        private void deletePrzedmiot()
        {
            int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            var przedmiot = dbContext.Przedmioty.First(c => c.ID_przedmiotu == id);
            dbContext.Przedmioty.Remove(przedmiot);
            dbContext.SaveChanges();
            studenciBindingSource.DataSource = dbContext.Przedmioty.ToList();
        }

        private void deleteSpecjalizacja()
        {
            int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            var specjalizacja = dbContext.Specjalizacja.First(c => c.ID_Specjalizacji == id);
            dbContext.Specjalizacja.Remove(specjalizacja);
            dbContext.SaveChanges();
            studenciBindingSource.DataSource = dbContext.Specjalizacja.ToList();
        }
        private void deleteProjekt()
        {
            int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            var projekt = dbContext.Projekty.First(c => c.ID_Projektu == id);
            dbContext.Projekty.Remove(projekt);
            dbContext.SaveChanges();
            studenciBindingSource.DataSource = dbContext.Projekty.ToList();
        }
        private void deleteOcenaPrzedmiot()
        {
            int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            var ocenaPrzedmiot = dbContext.Oceny_przedmioty.First(c => c.OPrz_Id == id);
            dbContext.Oceny_przedmioty.Remove(ocenaPrzedmiot);
            dbContext.SaveChanges();
            studenciBindingSource.DataSource = dbContext.Oceny_przedmioty.ToList();
        }

        private void deleteOcenaProjekt()
        {
            int id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            var ocenaProjekt = dbContext.Oceny_projekty.First(c => c.OProj_Id == id);
            dbContext.Oceny_projekty.Remove(ocenaProjekt);
            dbContext.SaveChanges();
            studenciBindingSource.DataSource = dbContext.Oceny_projekty.ToList();
        }
    }
}
