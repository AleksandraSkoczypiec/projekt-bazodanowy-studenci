﻿namespace Studenci
{
    partial class StudentControlPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ocenaPrzedmiotButton = new System.Windows.Forms.Button();
            this.ocenyProjektButton = new System.Windows.Forms.Button();
            this.deklaracjaProjektButton = new System.Windows.Forms.Button();
            this.witajLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ocenaPrzedmiotButton
            // 
            this.ocenaPrzedmiotButton.Location = new System.Drawing.Point(90, 137);
            this.ocenaPrzedmiotButton.Name = "ocenaPrzedmiotButton";
            this.ocenaPrzedmiotButton.Size = new System.Drawing.Size(91, 51);
            this.ocenaPrzedmiotButton.TabIndex = 0;
            this.ocenaPrzedmiotButton.Text = "Sprawdź oceny z przedmiotu";
            this.ocenaPrzedmiotButton.UseVisualStyleBackColor = true;
            this.ocenaPrzedmiotButton.Click += new System.EventHandler(this.ocenaPrzedmiotButton_Click);
            // 
            // ocenyProjektButton
            // 
            this.ocenyProjektButton.Location = new System.Drawing.Point(187, 137);
            this.ocenyProjektButton.Name = "ocenyProjektButton";
            this.ocenyProjektButton.Size = new System.Drawing.Size(91, 51);
            this.ocenyProjektButton.TabIndex = 1;
            this.ocenyProjektButton.Text = "Sprawdź oceny z projektów";
            this.ocenyProjektButton.UseVisualStyleBackColor = true;
            // 
            // deklaracjaProjektButton
            // 
            this.deklaracjaProjektButton.Location = new System.Drawing.Point(284, 137);
            this.deklaracjaProjektButton.Name = "deklaracjaProjektButton";
            this.deklaracjaProjektButton.Size = new System.Drawing.Size(91, 51);
            this.deklaracjaProjektButton.TabIndex = 2;
            this.deklaracjaProjektButton.Text = "Udział w projekcie";
            this.deklaracjaProjektButton.UseVisualStyleBackColor = true;
            // 
            // witajLabel
            // 
            this.witajLabel.AutoSize = true;
            this.witajLabel.Location = new System.Drawing.Point(209, 53);
            this.witajLabel.Name = "witajLabel";
            this.witajLabel.Size = new System.Drawing.Size(35, 13);
            this.witajLabel.TabIndex = 3;
            this.witajLabel.Text = "status";
            // 
            // StudentControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 327);
            this.Controls.Add(this.witajLabel);
            this.Controls.Add(this.deklaracjaProjektButton);
            this.Controls.Add(this.ocenyProjektButton);
            this.Controls.Add(this.ocenaPrzedmiotButton);
            this.Name = "StudentControlPanel";
            this.Text = "StudentControlPanel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ocenaPrzedmiotButton;
        private System.Windows.Forms.Button ocenyProjektButton;
        private System.Windows.Forms.Button deklaracjaProjektButton;
        private System.Windows.Forms.Label witajLabel;
    }
}