﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studenci
{
    public partial class OcenaPrzedmiot : Form
    {
        StudenciEntities1 dbContext = new StudenciEntities1();
        public OcenaPrzedmiot()
        {
            InitializeComponent();
            loadData();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            var ocenaPrzedmiot = new Oceny_przedmioty
            {
               ID_Studenta = (int)studentComboBox.SelectedValue,
               ID_przedmiotu = (int)przedmiotComboBox.SelectedValue,
               OPrz_Ocena = ocenaNumericUpDown.Value,
               Oprz_Data = dataOceny.Value

            };
            dbContext.Oceny_przedmioty.Add(ocenaPrzedmiot);
            dbContext.SaveChanges();
            this.Close();
        }
        private void loadData()
        {
            using (StudenciEntities1 c = new StudenciEntities1())
            {
                studentComboBox.ValueMember = "ID_Studenta";
                studentComboBox.DisplayMember = "ST_Nr_indeksu";
                studentComboBox.DataSource = c.Studenci.ToList();

                przedmiotComboBox.ValueMember = "ID_przedmiotu";
                przedmiotComboBox.DisplayMember = "Nazwa_przedmiotu";
                przedmiotComboBox.DataSource = c.Przedmioty.ToList();

            }
        }
    }
}
