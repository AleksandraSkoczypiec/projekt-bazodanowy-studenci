﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studenci
{
    public partial class ProwadzacyControlPanel : Form
    {
        public ProwadzacyControlPanel()
        {
            InitializeComponent();
        }

        private void tematProjektuButton_Click(object sender, EventArgs e)
        {
            DataWindow dataWindow = new DataWindow(DbDataType.Projekty);
            dataWindow.Show();
        }

        private void ocenyPrzedmiotButton_Click(object sender, EventArgs e)
        {
            DataWindow dataWindow = new DataWindow(DbDataType.Ocena_przedmiot);
            dataWindow.Show();
        }

        private void ocenyProjektButton_Click(object sender, EventArgs e)
        {
            DataWindow dataWindow = new DataWindow(DbDataType.Ocena_projekt);
            dataWindow.Show();
        }
    }
}
